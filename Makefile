SHELL := /bin/bash
NODE_VERSION="16"

nvm_install:
	chmod +x ./scripts/install_nvm.sh && ./scripts/install_nvm.sh

nvm_yarn_watch:
	chmod +x ${NVM_DIR}/nvm.sh && \. ${NVM_DIR}/nvm.sh && nvm use ${NODE_VERSION} && yarn watch

nvm_install_watch: nvm_install yarn_watch

watch:
	yarn watch

