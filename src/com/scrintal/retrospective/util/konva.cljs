(ns com.scrintal.retrospective.util.konva
  (:require ["konva"]
            [reagent.core :as r]))

(defn ->Stage [args]
  (js/Konva.Stage. (clj->js args)))

(defn ->Layer [args]
  (js/Konva.Layer. (clj->js args)))

(defn ->Group [args]
  (js/Konva.Group. (clj->js args)))

(defn ->Rect [args]
  (js/Konva.Rect. (clj->js args)))

(defn ->Text [args]
  (js/Konva.Text. (clj->js args)))

(defn ->Line [args]
  (js/Konva.Line. (clj->js args)))

(defn set-size! [^js/Konva.Node node size]
  (.size node (clj->js size)))

(defn destroy! [^js/Konva.Node node]
  (.destroy node))

(defn get-stage [^js/Konva.Node node]
  (.getStage node))

(defn off [^js/Konva.Node node event-name]
  (.off node event-name))

(defn on [^js/Konva.Node node event-name event-handler]
  (.on node event-name event-handler))

(defn set-text! [^js/Konva.Text text-node text]
  (.text text-node text))

(defn set-position! [^js/Konva.Node node position]
  (.position node (clj->js position)))

(defn get-position [^js/Konva.Node node]
  (.position node))

(defn set-absolute-position! [^js/Konva.Node node position]
  (.absolutePosition node position))

(defn get-absolute-position [^js/Konva.Node node]
  (.getAbsolutePosition node))

(defn add-child! [^js/Konva.Node parent-node ^js/Konva.Node child-node]
  (.add parent-node child-node))

(defn remove! [^js/Konva.Node node]
  (.remove node))

(defn remove-children! [^js/Konva.Node node]
  (.removeChildren node))

(defn find-one-by-name [^js/Konva.Node node name]
  (.findOne node (str "." name)))

(defn find-all-by-name [^js/Konva.Node node name]
  (.find node (str "." name)))

(defn find-one-by-id [^js/Konva.Node node id]
  (.findOne node (str "#" id)))

(defn- get-children [^js/Konva.Node node fn]
  (.getChildren node fn))

(defn emit-event! [^js/Konva.Stage stage event-type event-data]
  (when stage
    (when-let [on-change (.getAttr stage "onChangeHandler")]
      (on-change stage {:type event-type :data event-data}))))

(defn attach-on-change-handler! [^js/Konva.Stage stage on-change]
  (.setAttr stage "onChangeHandler" (clj->js on-change)))

(def ^:private title-width 400)
(def ^:private title-offset 32)
(def ^:private title-margin 64)

(defn- create-title
  "Creates a title element for the retrospective canvas. Each title will have a
   Text element that contains the title text and a line underneath indicating a
   title column. Position is the column position, starting from 0."
  [text color position]
  (let [x           (+ title-offset (* position (+ title-width title-margin)))
        y           title-offset
        title-group (->Group {:x x :y y})
        title       (->Text {:x          0
                             :y          0
                             :lineHeight 1.7
                             :fontSize   24
                             :fill       color
                             :text       text})
        line        (->Line {:points      [0 48 title-width 48]
                             :stroke      color
                             :strokeWidth 2})]
    (add-child! title-group title)
    (add-child! title-group line)
    title-group))

(defn- generate-text-area
  ""
  [parameters]
  (let [text-area (js/document.createElement "textarea")]

    (set! (.-className text-area) "post-it-edit")

    (set! (.-value text-area) (:value parameters))
    (set! (.. text-area -style -position) (:position parameters))
    (set! (.. text-area -style -resize) (:resize parameters))


    (set! (.. text-area -style -top)
          (cljs.pprint/cl-format nil "~apx"  (:top parameters)))
    (set! (.. text-area -style -left)
          (cljs.pprint/cl-format nil "~apx" (:left parameters)))

    text-area))

(defn- get-group-from-event ""
  [^js event]
  (.-parent (.-target event)))

(defn- delete-post-it ""
  [^js event]
  (let [group (get-group-from-event event)]
    (emit-event! (.getStage group) :remove-post-it {:id (.id group)})
    (destroy! group)))

(defonce current-group (r/atom (->Group {:draggable true})))

(defn- in-current-group? ""
  [group]
  (not-empty
   (get-children @current-group
                 (fn [child] (when (= group child) true)))))

(defn- group-move-children ""
  [group new-group position]
  (let [post-it (first (.-children group))]
    (.draggable group true)
    (.shadowColor post-it "grey")
    (.moveTo group new-group)
    (set-absolute-position! group position)))

(defn- select-group ""
  [^js event]
  (let [group (get-group-from-event event)
        post-it (first (.-children group))
        layer (.getLayer group)
        current-pos (.getAbsolutePosition group )]

    (if (in-current-group? group)
      (group-move-children group layer current-pos)
      (do
        (.draggable group false)
        (.shadowColor post-it "blue")
        (add-child! @current-group group)
        (set-absolute-position! group current-pos)))))

(defn- on-drop-group ""
  [^js event]
  (let [group (.-target event)
        children (.getChildren group)]
    ;;TODO: This is a hack, for some reason, the doseq iteration
    ;; doesn't remove all the children in the first "try"
    (while (not-empty children)
      (doseq [^js/Konva.Node child children]
        (let [layer (.getLayer child)
              current-pos (.getAbsolutePosition child)]
          (group-move-children child layer current-pos))))))

(defn- edit-post-it ""
  [^js event]
  (let [group (get-group-from-event event)
        stage (.-parent (.-parent group))
        stage-box (.getBoundingClientRect (.container stage))

        text-node (last (.-children group))
        text-absolute (get-absolute-position text-node)

        area-position {:x (+ (.-left stage-box) (.-x text-absolute))
                       :y (+ (.-top stage-box) (.-y text-absolute))}

        text-area (generate-text-area {:value (.text text-node)
                                       :position "absolute"
                                       :rezie "none"
                                       :top (:y area-position)
                                       :left (:x area-position)})]
    (.addEventListener text-area "keydown"
                       (fn [e]
                         (when (= (.-keyCode e) 13)
                           (let [text (.-value text-area)]
                             (set-text! text-node text)
                             (emit-event! stage :update-post-it
                                          {:id (.id group)
                                           :type "update-text"
                                           :text text})
                             (js/document.body.removeChild text-area)
                             (.listening group true)
                             (.show text-node)))))

    (.listening group false)
    (.hide text-node)

    (js/document.body.appendChild text-area)
    (.focus text-area)))


(defn- emit-group-position! ""
  [^js/Konva.Node group]
  (let [type "update-position" 
        position (get-position group)]
    (emit-event! (.getStage group) :update-post-it
                 {:id (.id group)
                  :type type
                  :position position})))

(defn- on-drag-move-group ""
  [^js event]
  (emit-group-position! (.-target event)))

(defn- on-drag-move-current-group ""
  [^js event]
  (let [group (.-target event)
        children (.getChildren group)]
    (doseq [^js/Konva.Node child children]
      (emit-group-position! child))))

(defn- on-left-click-group
  ""
  [^js event]
  (let [group (get-group-from-event event)
        mouse-event (.-evt event)
        alt-key (.-altKey mouse-event)
        shift-key (.-shiftKey mouse-event)
        control-key (.-ctrlKey mouse-event)]
    (cond
      (and alt-key
           (js/confirm "Do you want to delete the post-it?"))
      (delete-post-it event)

      (and shift-key (not
                      (in-current-group? group)))
      (edit-post-it event)

      control-key (select-group event))))

(defn dispatch-element-type [^js _stage _id {:keys [type]}]
  type)


(defn- on-zoom ""
  [^js event]
  (.. event -evt preventDefault)
  (let [stage (.. event -target getStage)
        scale-by 1.01
        old-scale (.scaleX stage)
        pointer (.getPointerPosition stage)
        mouse-point-to {:x (/ (- (.-x pointer) (.x stage)) old-scale)
                        :y (/ (- (.-y pointer) (.y stage)) old-scale)}
        direction (or (and (> (.-deltaY (.-evt event)) 0) 1) -1)
        new-scale (or (and (> direction 0)
                           (* old-scale scale-by))
                      (/ old-scale scale-by))]
    (.scale stage
            (clj->js
             {:x new-scale
              :y new-scale}))

    (set-position! stage
                   {:x (- (.-x pointer) (*  (:x mouse-point-to) new-scale)) 
                    :y (- (.-y pointer) (*  (:y mouse-point-to) new-scale))})))

(defmulti upsert-element!
  "Upserst an element with the given id and data."
  {:arglists '([stage id element-data])} dispatch-element-type)

(defmethod upsert-element! "post-it" [^js stage id {:keys [position]}]
  (let [group (->Group {:id         id
                        :draggable true})
        layer (first (.getChildren stage))
        text (->Text
              {:x          (- (js/Number (:x position)) 100)
               :y          (- (js/Number (:y position)) 100)
               :lineHeight 1.4
               :text       ""
               :fontSize 14
               :fontFamily "sans-serif"
               :width 200
               :height 200
               :text-align "left"
               :padding 10})
        post-it (->Rect
                 {:x (:x position)
                  :y (:y position)
                  :width 200
                  :height 200
                  :fill "yellow"
                  :strokeWidth 4
                  :offsetX 100
                  :offsetY 100
                  :shadowBlur 10
                  :shadowColor "grey"})]
    (on group "click" on-left-click-group)
    (on group "dragmove" on-drag-move-group)
    (add-child! group post-it)
    (add-child! group text)
    (add-child! layer group)))

(defmethod upsert-element! "update-position" [^js stage id {:keys [position]}]
  (let [group (find-one-by-id stage id)
        type "post-it"]
    (if group
      (set-position! group position)
      (upsert-element! stage id {:type type :position position}))))

(defmethod upsert-element! "update-text" [^js stage id {:keys [text]}]
  (let [group (find-one-by-id stage id)
        text-node (last (.-children group))]
    (set-text! text-node text)))

(defmethod upsert-element! :default [^js _stage id {:keys [type]}]
  (js/console.warn "Unknown element type upserted! Type:" type "Id:" id))

(defn- on-double-click-stage ""
  [^js event]
  (let [stage    (.-target event)
        position {:x (/ (.width stage) 2)
                  :y (/ (.height stage) 2)}
        id       (str (random-uuid))
        type     "post-it"]
    (when (instance? js/Konva.Stage stage)
      (upsert-element! stage id {:type     type :position position})
      (emit-event! stage :update-post-it {:id       id
                                          :type     type
                                          :position position}))))

(defn init!
  "Initializes the Konva stage."
  [^js canvas-root on-change width height]
  (let [stage    (->Stage {:name      "stage"
                           :container canvas-root
                           :width     width
                           :height    height
                           :scaleX    1
                           :scaleY    1
                           :x         0
                           :y         0
                           :draggable true})
        layer    (->Layer {:name "layer"})
        start    (create-title "Start" "#3EDE54" 0)
        stop     (create-title "Stop" "#FF4732" 1)
        continue (create-title "Continue" "#FFCE4F" 2)]
    (add-child! layer start)
    (add-child! layer stop)
    (add-child! layer continue)
    (add-child! layer @current-group)
    (add-child! stage layer)
    (attach-on-change-handler! stage on-change)

    (on @current-group "dragend" on-drop-group)
    (on @current-group "dragmove" on-drag-move-current-group)
    (on stage "wheel" on-zoom)
    (on stage "dblclick" on-double-click-stage)
    stage))
