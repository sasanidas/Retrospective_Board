(ns com.scrintal.retrospective.util.yjs.interface.provider)

(defprotocol Provider
  (get-doc [provider]
    "Returns the underlying YDoc instance")

  (provider* [provider]
    "Returns the underlying provider instance.")

  (get-options [provider]
    "Returns the original options provided to create this provider.")

  (destroy! [provider]
    "Destroys the underlying provider instance.")

  (destroyed? [provider]
    "Checks if this provider is already destroyed or not."))
