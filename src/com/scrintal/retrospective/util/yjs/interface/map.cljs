(ns com.scrintal.retrospective.util.yjs.interface.map
  (:refer-clojure :exclude [get set! keys vals]))

(defprotocol YMap
  "Represents a Y.js Map and provides access methods
   to underlying implementation.
   
   Refer to: https://docs.yjs.dev/api/shared-types/y.map#api"

  (map* [y-map]
    "Returns the underlying Y.Map instance.")

  (doc [y-map]
    "Returns the underlying doc.")

  (get [y-map key]
    "Returns an entry with the specified key.")

  (set! [y-map key value]
    "Add or update an entry with the specified key.")

  (delete! [y-map key]
    "Deletes an entry with the specified key.")

  (has? [y-map key]
    "Returns true if an entry with the specified key exists.")

  (size [y-map]
    "Returns the number of key/value pairs")

  (->json [y-map]
    "Copies the key/value pairs of this Y.Map to a new Object.
     It transforms all shared types to JSON using their toJSON
     method.")

  (->clj
    [y-map]
    [y-map opts]
    "Copies the key/value pairs of this Y.Map to a new hash-map.
    
     Available opts:
       keywordize-keys: boolean, defaults to true")

  (keys [y-map]
    "Returns a vector of all keys of this Y.Map.")

  (vals
    [y-map]
    [y-map opts]
    "Returns a vector of all values of this Y.Map.
     
     Available opts:
       keywordize-keys: boolean, defaults to true")

  (for-each [y-map callback-fn]
    "Iterates over the y-map entries and calls the callback-fn for
     each of them. You may provide a callback-fn with value, key, and
     map arguments.")

  (observe-deep [y-map callback-fn]
    "Registers a change observer that will be called synchronously 
     every time this type or any of its children is modified. If
     there is an existing observer, it will be removed before adding
     the new observer.")

  (unobserve-deep [y-map]
    "Unregisters the change observer that has been registered with
     observe-deep, if any."))

(defprotocol YMapOperations
  (undo-manager [y-map tracked-origins]
    "Creates a new Y.UndoManager on the scope of this shared type. If 
     any of the specified type, or any of its children are modified, 
     the UndoManager adds a reverse-operation on its stack.
     
     Refer to: https://docs.yjs.dev/api/undo-manager"))
