(ns com.scrintal.retrospective.util.yjs.interface.undo-manager)

(defprotocol YUndoManager
  "Represents a Y.UndoManager and provides access methods to
   underlying implementation.
   
   Refer to: https://docs.yjs.dev/api/undo-manager"

  (undo-manager* [y-undo-manager]
    "Returns the underlying Y.UndoManager instance.")

  (undo! [y-undo-manager]
    "Undo the last operation on the UndoManager stack. The 
     reverse operation will be put on the redo-stack.")

  (redo! [y-undo-manager]
    "Redo the last operation on the redo-stack. I.e. the 
     previous redo is reversed.")

  (clear! [y-undo-manager]
    "Delete all captured operations from the undo & redo stack.")

  (stop-capturing [y-undo-manager]
    "Call stop-capturing to ensure that the next operation 
     that is put on the UndoManager is not merged with the 
     previous operation."))
