(ns com.scrintal.retrospective.util.yjs.interface.transaction)

(defprotocol YTransaction
  "Represents a Y.js transaction and provides access methods to
   underlying implementation."

  (transaction* [y-transaction]
    "Returns the underlying transaction instance.")

  (origin [y-transaction]
    "Returns the origin of the transaction, if any. Origin is passed
     as an optional argument when you call Y.Doc's transact function.")

  (initial? [y-transaction]
    "Returns true if this is the initial transaction, which means the
     state before this transaction had a size of 0.")

  (local? [y-transaction]
    "Returns true if this transaction is a local transaction."))
