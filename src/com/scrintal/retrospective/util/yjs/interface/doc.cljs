(ns com.scrintal.retrospective.util.yjs.interface.doc)

(defprotocol YDoc
  "Represents a Y.js document and provides access methods 
   to underlying implementation.
   
   Refer to: https://docs.yjs.dev/api/y.doc#y.doc-api"

  (doc* [y-doc]
    "Returns the underlying Y.Doc instance.")

  (client-id [y-doc]
    "Returns the client id of the underlying Y.Doc instance.")

  (transact!
    [y-doc callback-fn]
    [y-doc callback-fn origin]
    "Bundles the changes that happen within the callback-fn into
     a single transaction. Accepts an optional origin parameter
     which can be accessed via (.-origin transaction).")

  (get-map [y-doc key]
    "Defines a shared Y.Map type with the given key and returns a YMap
     instance.")

  (get-xml-fragment [y-doc key]
    "Defines a shared Y.XmlFragment type with the given key.")

  (destroy! [y-doc]
    "Destroys the Y.Doc instance."))

(defprotocol YDocOperations
  (apply-update
    [y-doc update]
    [y-doc update origin]
    "Apply a document update on the shared document. Accepts an optional
     origin parameter which will be available on update event.
                 
     Refer to: https://docs.yjs.dev/api/document-updates#update-api")

  (encode-state-as-update [y-doc]
    "Encode the document state as a single update message that
     can be applied on the remote document.
     
     Refer to: https://docs.yjs.dev/api/document-updates#update-api"))

(defprotocol YDocObservable
  (on [y-doc name callback-fn]
    "Registers an event handler with the given name and callback function.")

  (off [y-doc name callback-fn]
    "Unregisters the event handler with the given name and callback function."))
