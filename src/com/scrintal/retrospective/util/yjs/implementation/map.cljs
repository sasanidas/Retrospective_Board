(ns com.scrintal.retrospective.util.yjs.implementation.map
  (:require ["yjs" :refer [Map AbstractType]]
            [com.scrintal.retrospective.util.yjs.interface.map :as y.map]
            [com.scrintal.retrospective.util.yjs.implementation.undo-manager :as undo-manager-impl]))

(def create-event!
  (delay (resolve 'com.scrintal.retrospective.util.yjs.implementation.event/->YEventImpl)))

(def create-transaction!
  (delay (resolve 'com.scrintal.retrospective.util.yjs.implementation.transaction/->YTransactionImpl)))

(def create-doc!
  (delay (resolve 'com.scrintal.retrospective.util.yjs.implementation.doc/->YDocImpl)))

(defn- change-observer [y-map callback-fn ^js events ^js transaction]
  (callback-fn
   {:y-map       y-map
    :events      (mapv @create-event! events)
    :transaction (@create-transaction! transaction)}))

(declare YMapImpl)

(defn parse-value [value]
  (if (instance? Map value)
    (YMapImpl. value)
    (if (instance? AbstractType value)
      value
      (js->clj value :keywordize-keys true))))

(defrecord YMapImpl [^js y-map]
  y.map/YMap
  (map* [_]
    y-map)

  (doc [_]
    (@create-doc! (.-doc y-map)))

  (get [_ key]
    (let [value (.get y-map key)]
      (parse-value value)))

  (set! [_ key value]
    (.set y-map key
          (if (satisfies? y.map/YMap value)
            (y.map/map* value)
            (clj->js value))))

  (delete! [_ key]
    (.delete y-map key))

  (has? [_ key]
    (.has y-map key))

  (size [_]
    (.-size y-map))

  (keys [_]
    (-> y-map
        (.keys)
        (js/Array.from)
        (js->clj)))

  (vals [_ {:keys [keywordize-keys]
            :or   {keywordize-keys true}}]
    (-> y-map
        (.values)
        (js/Array.from)
        (js->clj :keywordize-keys keywordize-keys)))

  (for-each [_ callback-fn]
    (.forEach y-map
              (fn [value key map]
                (callback-fn (parse-value value) key (YMapImpl. map)))))

  (vals [this]
    (y.map/vals this nil))

  (->json [_]
    (.toJSON y-map))

  (->clj [this {:keys [keywordize-keys]
                :or   {keywordize-keys true}}]
    (let [json (y.map/->json this)]
      (js->clj json :keywordize-keys keywordize-keys)))

  (->clj [this]
    (y.map/->clj this nil))

  (observe-deep [this callback-fn]
    (y.map/unobserve-deep this)
    (let [observer-fn (partial change-observer this callback-fn)]
      (set! (.-scrintalObserverFn y-map) observer-fn)
      (.observeDeep y-map observer-fn)))

  (unobserve-deep [_]
    (when-let [observer-fn (.-scrintalObserverFn y-map)]
      (.unobserveDeep y-map observer-fn)
      (set! (.-scrintalObserverFn y-map) nil)))

  y.map/YMapOperations
  (undo-manager [_ tracked-origins]
    (undo-manager-impl/create! y-map tracked-origins)))

(defn create! ^YMapImpl []
  (->YMapImpl (Map.)))
