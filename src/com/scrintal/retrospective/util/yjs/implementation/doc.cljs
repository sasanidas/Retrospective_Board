(ns com.scrintal.retrospective.util.yjs.implementation.doc
  (:require ["yjs" :refer [encodeStateAsUpdate applyUpdate Doc]]
            [com.scrintal.retrospective.util.yjs.interface.doc :as y.doc]
            [com.scrintal.retrospective.util.yjs.implementation.map :as map-impl]))

(defrecord YDocImpl [^js y-doc]
  y.doc/YDoc
  (doc* [_]
    y-doc)

  (client-id [_]
    (.-clientID y-doc))

  (transact! [_ callback-fn]
    (.transact y-doc callback-fn))

  (transact! [_ callback-fn origin]
    (.transact y-doc callback-fn origin))

  (get-map [_ key]
    (map-impl/->YMapImpl (.getMap y-doc key)))

  (get-xml-fragment [_ key]
    (.getXmlFragment y-doc key))

  (destroy! [_]
    (.destroy y-doc))

  y.doc/YDocOperations
  (apply-update [_ update origin]
    (applyUpdate y-doc update origin))

  (apply-update [_ update]
    (applyUpdate y-doc update))

  (encode-state-as-update [_]
    (encodeStateAsUpdate y-doc))

  y.doc/YDocObservable
  (on [_ name callback-fn]
    (.on y-doc name callback-fn))

  (off [_ name callback-fn]
    (.off y-doc name callback-fn)))

(defn create! ^YDocImpl []
  (->YDocImpl (Doc.)))
