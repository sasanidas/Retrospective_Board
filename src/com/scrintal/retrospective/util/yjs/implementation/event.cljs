(ns com.scrintal.retrospective.util.yjs.implementation.event
  (:require ["yjs" :refer [Map AbstractType]]
            [com.scrintal.retrospective.util.yjs.interface.event :as y.event]
            [com.scrintal.retrospective.util.yjs.implementation.map :as map-impl]))

(defrecord YEventImpl [^js y-event]
  y.event/YEvent
  (event* [_]
    y-event)

  (target [_]
    (let [^js target (.-target y-event)]
      (if (instance? Map target)
        (map-impl/->YMapImpl target)
        target)))

  (parent [_]
    (let [^js parent (.. y-event -target -parent)]
      (if (instance? Map parent)
        (map-impl/->YMapImpl parent)
        parent)))

  (root? [_]
    (nil? (.. y-event -target -parent)))

  (changed-keys [_]
    (-> y-event
        (.-keysChanged)
        (js/Array.from)
        (into #{})))

  (changes [_ callback-fn]
    (let [^js keys (.. y-event -changes -keys)]
      (.forEach keys (fn [^js change key]
                       (let [action    (keyword (.-action change))
                             old-value (.-oldValue change)]
                         (callback-fn
                          (cond-> {:action action
                                   :key    key}

                            (or (= :delete action) (= :update action))
                            (assoc :old-value (if (instance? Map old-value)
                                                (map-impl/->YMapImpl old-value)
                                                (if (instance? AbstractType old-value)
                                                  old-value
                                                  (js->clj old-value :keywordize-keys true)))))))))))

  (path [_]
    (-> y-event
        (.-path)
        (js->clj))))
