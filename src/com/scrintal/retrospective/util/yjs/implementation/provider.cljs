(ns com.scrintal.retrospective.util.yjs.implementation.provider
  (:require ["y-webrtc" :refer [WebrtcProvider]]
            [com.scrintal.retrospective.util.yjs.implementation.doc :as doc-impl]
            [com.scrintal.retrospective.util.yjs.interface.doc :as y.doc]
            [com.scrintal.retrospective.util.yjs.interface.provider :as y.provider]))

;; y-doc:    com.scrintal.retrospective.util.yjs.interface.doc/YDoc
;; y-webrtc: https://github.com/yjs/y-webrtc
(defrecord WebrtcProviderImpl [room-name
                               y-doc
                               ^js webrtc
                               options]
  y.provider/Provider
  (get-doc [_provider]
    y-doc)

  (get-options [_provider]
    options)

  (provider* [_provider]
    webrtc)

  (destroy! [_provider]
    (set! (.-isDestroyed webrtc) true)
    (.destroy webrtc))

  (destroyed? [_provider]
    (true? (.-isDestroyed webrtc))))

(defn create!
  "Creates a new WebrtcProviderImpl. Check: yjs/y-webrtc for more information
   https://github.com/yjs/y-webrtc"
  [room-name options]
  (let [y-doc    (doc-impl/create!)
        ^js webrtc   (WebrtcProvider. room-name (y.doc/doc* y-doc) (clj->js options))
        provider (->WebrtcProviderImpl room-name y-doc webrtc options)]
    provider))
