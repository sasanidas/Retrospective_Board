(ns com.scrintal.retrospective.util.yjs.implementation.undo-manager
  (:require ["yjs" :refer [UndoManager AbstractType]]
            [com.scrintal.retrospective.util.yjs.interface.undo-manager :as y.undo-manager]))

(defrecord YUndoManagerImpl [^js y-undo-manager]
  y.undo-manager/YUndoManager
  (undo-manager* [_]
    y-undo-manager)

  (undo! [_]
    (.undo y-undo-manager))

  (redo! [_]
    (.redo y-undo-manager))

  (clear! [_]
    (.clear y-undo-manager))

  (stop-capturing [_]
    (.stopCapturing y-undo-manager)))

(defn create! ^YUndoManagerImpl [^AbstractType y-type tracked-origins]
  (let [tracked-origins (into [nil] tracked-origins)]
    (->YUndoManagerImpl (UndoManager. y-type #js {:trackedOrigins (js/Set. (clj->js tracked-origins))}))))
