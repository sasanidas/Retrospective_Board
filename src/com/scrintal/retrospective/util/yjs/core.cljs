(ns com.scrintal.retrospective.util.yjs.core
  (:require [com.scrintal.retrospective.util.yjs.implementation.doc]
            [com.scrintal.retrospective.util.yjs.implementation.event]
            [com.scrintal.retrospective.util.yjs.implementation.map :as map]
            [com.scrintal.retrospective.util.yjs.implementation.provider :as provider]
            [com.scrintal.retrospective.util.yjs.implementation.transaction]
            [com.scrintal.retrospective.util.yjs.implementation.undo-manager]
            [com.scrintal.retrospective.util.yjs.interface.map :as y.map]))

(defn create-provider!
  ([room-name options]
   (provider/create! room-name options))
  ([room-name]
   (create-provider! room-name {})))

(defn create-map! []
  (map/create!))

(defn map->y-map [map]
  (let [y-map (create-map!)]
    (doseq [[k v] map]
      (let [k (name k)]
        (if (not (nil? v))
          (y.map/set! y-map k v)
          (y.map/delete! y-map k))))
    y-map))
