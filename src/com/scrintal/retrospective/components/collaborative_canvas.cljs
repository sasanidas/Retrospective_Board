(ns com.scrintal.retrospective.components.collaborative-canvas
  (:require [clojure.string :as str]
            [com.scrintal.retrospective.components.canvas :as canvas]
            [com.scrintal.retrospective.util.yjs.core :as yjs]
            [com.scrintal.retrospective.util.yjs.interface.doc :as y.doc]
            [com.scrintal.retrospective.util.yjs.interface.map :as y.map]
            [com.scrintal.retrospective.util.yjs.interface.provider :as y.provider]
            [reagent.core :as r]))

;; Keep track of active providers so that we can destroy them
;; when the code is hot-reloaded. Dev-only.
(defonce active-providers (atom #{}))

#_{:clj-kondo/ignore [:unused-private-var]}
(defn- ^:dev/before-load clean-up!
  "Cleanup active providers when hot-reloaded."
  []
  (doseq [provider @active-providers]
    (y.provider/destroy! provider)))

(defn render [{:keys [room-name password]}
              on-change-canvas
              on-change-y-map]
  (r/with-let [stage    (atom nil)
               provider (yjs/create-provider!
                         room-name (cond-> {}
                                     (not (str/blank? password))
                                     (assoc :password password)))
               y-doc    (y.provider/get-doc provider)
               y-map    (y.doc/get-map y-doc "elements")
               _        (swap! active-providers conj provider)
               on-stage-ready (fn [^js current-stage]
                                (reset! stage current-stage)
                                (y.map/observe-deep y-map (partial on-change-y-map current-stage)))
               on-change (partial on-change-canvas y-map)]
    [:<>
     [canvas/render
      {:on-stage-ready on-stage-ready
       :on-change      on-change}]
     [:p {:class "room-name"} room-name]]
    (finally
      (when-not (y.provider/destroyed? provider)
        (y.provider/destroy! provider))
      (y.map/unobserve-deep y-map)
      (swap! active-providers disj provider))))
