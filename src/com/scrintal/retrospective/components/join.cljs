(ns com.scrintal.retrospective.components.join
  (:require [clojure.string :as str]
            [reagent.core :as r]))

(defn render [on-submit]
  (r/with-let [on-submit-form (fn [^js event]
                                (.preventDefault event)
                                (let [form-data (js/FormData. (.-target event))
                                      room-name (.get form-data "room-name")
                                      password  (.get form-data "password")]
                                  (when-not (str/blank? room-name)
                                    (on-submit
                                     (cond-> {:room-name (-> room-name
                                                             (str/trim)
                                                             (str/lower-case))}
                                       (not (str/blank? password))
                                       (assoc :password password))))))]
    [:div {:class "join"}
     [:h1 {:class "title"}
      "Join Retrospective"]
     [:form {:class     "join-form"
             :on-submit on-submit-form}
      [:label {:for "room-name"} "Room Name*"]
      [:input {:type         "text"
               :id           "room-nam"
               :name         "room-name"
               :required     true
               :autoComplete "off"
               :placeholder  "A unique room name"
               :defaultValue (str (random-uuid))}]
      [:label {:for "password"} "Password"]
      [:input {:type         "password"
               :id           "pasword"
               :name         "password"
               :required     false
               :autoComplete "off"
               :placeholder  "Password (Optional)"}]
      [:input {:type  "submit"
               :value "Join"}]]]))
